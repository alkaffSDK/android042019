package com.sdkjordan.a.alkaff.android042019.activities;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sdkjordan.a.alkaff.android042019.R;
import com.sdkjordan.a.alkaff.android042019.adapters.MyCustomAdapterWithViewHolder;
import com.sdkjordan.a.alkaff.android042019.helpers.SQLHelper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Map;

public class ContactsActivity extends AppCompatActivity implements View.OnClickListener {


    private static final String FILE = "data.txt" ;
    private static final String TAG =  "ContactsActivity";
    private static final int READ_CONTACTS_PERMISSION_REQ_ID = 20 ;
    private EditText mEditTextName;
    private EditText mEditTextPhone;
    private ListView mListView;

    private SharedPreferences mPreferences;

    private SQLHelper mHelper ;

    private MyCustomAdapterWithViewHolder adapter;

    private ContentResolver mContentResolver ;

    private SimpleCursorAdapter mSimpleCursorAdapter ;
    private String[] from = {SQLHelper.COLUMN_NAME , SQLHelper.COLUMN_PHONE};
    private int[] to = {R.id.textViewName , R.id.textViewMobile};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(this);


        mHelper = new SQLHelper(getApplicationContext());

        mContentResolver = getContentResolver() ;

        // Initialize the shared preferences
        // will create Shared Preferences file  named as the activity "ContactsActivity.xml"
        mPreferences = getPreferences(MODE_PRIVATE);

        // OR you can use
        //mPreferences = getSharedPreferences(Constants.SHARED_PREFRENCES_FILE, MODE_PRIVATE);
        // will create Shared Preferences file  named as the the name you provide "MyPrefs.xml"


        mEditTextName = findViewById(R.id.editTextName);
        mEditTextPhone = findViewById(R.id.editTextPhone);
        mListView = findViewById(R.id.list_view_contacts);

        Cursor c = mHelper.getAllContacts();

        mSimpleCursorAdapter = new SimpleCursorAdapter(getApplicationContext(),R.layout.list_item3,c, from , to,SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        adapter = new MyCustomAdapterWithViewHolder(getApplicationContext());
        //loadDataFromFile(FILE,adapter);
        //loadDataFromSharedPreferences(mPreferences, adapter);
        mListView.setAdapter(mSimpleCursorAdapter);


        //copyImageFromResources(R.raw.android3 , "android.jpg");

        if(hasReadContactPermissions())
            loadDataFromContacts();

    }

    private boolean hasReadContactPermissions() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS))
                    Toast.makeText(this, "This application needs to read your contacts to operate", Toast.LENGTH_SHORT).show();

                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, READ_CONTACTS_PERMISSION_REQ_ID);
                return  false ;
            }
        }
        return  true ;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == READ_CONTACTS_PERMISSION_REQ_ID)
        {
            for(int i=0;i < grantResults.length;i++)
            {
                if(grantResults[i] == PackageManager.PERMISSION_DENIED)
                    return;
            }

            loadDataFromContacts();
        }

    }

    private  void loadDataFromContacts()
    {
        // Content Resolver

        Log.d(TAG, "Start loading...") ;
        String[] selecPhones =  {ContactsContract.CommonDataKinds.Phone.CONTACT_ID, ContactsContract.CommonDataKinds.Phone.NUMBER};

        String[] selection = {ContactsContract.Contacts._ID , ContactsContract.Contacts.DISPLAY_NAME , ContactsContract.Contacts.HAS_PHONE_NUMBER};
        Cursor contacts = mContentResolver.query(ContactsContract.Contacts.CONTENT_URI,selection,null,null,null);
        Uri url = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String contactID = null, displayName;
        boolean hasNumber ;
         int count ;
        String where = ContactsContract.Contacts.Entity.CONTACT_ID + " = ?";

        StringBuilder builder  = new StringBuilder()  ;
        if(contacts != null)
        {
            while (contacts.moveToNext()) {
                contactID = contacts.getString(0) ;
                displayName = contacts.getString(1) ;
                String has = contacts.getString(2);
                hasNumber = has.equals("1");

                if(hasNumber)
                {
                    Cursor c =  mContentResolver.query(url, selecPhones, where , new String[]{contactID},null);
                    if(c != null)
                    {
                        count = c.getColumnCount() ;
                        while (c.moveToNext())
                            builder.append(c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))) ;

                    }else {
                        Log.d(TAG, String.format("%-5s : %-20s , %5s {No info}",contactID,displayName,hasNumber)) ;
                    }

                    Log.d(TAG, String.format("%-5s : %-20s , %5s {%s}",contactID,displayName,hasNumber, builder.substring(0, builder.length()-2))) ;
                    builder = new StringBuilder();
                }else {
                    Log.d(TAG, String.format("%-5s : %-20s , %5s {No info}",contactID,displayName,hasNumber)) ;
                }
            }

        }else {
            Log.d(TAG,"No Contacts") ;
        }






    }


    private void loadDataFromSharedPreferences(SharedPreferences mPreferences, MyCustomAdapterWithViewHolder adapter) {
        String[] split;

        for (String key : mPreferences.getAll().keySet()) {
            split = mPreferences.getString(key, null).split(",");
            if (split != null && split.length > 1)
                adapter.add(split[0], split[1],false);
        }

        adapter.notifyDataSetChanged();

    }

    private void saveToSharedPreferences(SharedPreferences mPreferences, int id, String name, String phone) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(String.valueOf(id), name + "," + phone);

        // to save data to the xml file
        editor.apply();         // or editor.commit()

    }

    private void saveToInternalFile1(String filename, int id, String name, String phone) {
        try (FileOutputStream fos = openFileOutput(filename, MODE_APPEND)) {
            fos.write((id + "," + name + "," + phone+"\n").getBytes());
//            fos.flush();
        } catch (IOException e) {e.printStackTrace();}
    }
    private void saveToInternalFile(String filename, int id, String name, String phone) {
        try (PrintWriter pr = new PrintWriter(openFileOutput(filename, MODE_APPEND)) ) {
            pr.println(id + "," + name + "," + phone);
        } catch (FileNotFoundException e) {e.printStackTrace();}
    }
    private void loadDataFromFile(String filename ,MyCustomAdapterWithViewHolder adapter)
    {
     try(BufferedReader reader = new BufferedReader(new InputStreamReader(openFileInput(filename))) )
     {
         String line = "";
         String[] split ;
         while ((line = reader.readLine()) != null)
         {
             split = line.split(",");
             if(split != null && split.length > 2)
             {
                 adapter.add(split[1],split[2],false);
             }
         }
         adapter.notifyDataSetChanged();
     } catch (IOException e) {e.printStackTrace();}
    }

    @Override
    public void onClick(View v) {
        String name = mEditTextName.getText().toString();
        String phone = mEditTextPhone.getText().toString();

        //saveToInternalFile(FILE,adapter.getCount(),name,phone);
        //saveToSharedPreferences(mPreferences, adapter.getCount(), name, phone);

        //adapter.add(name, phone,true);
        mHelper.insertContact(name,phone);

        // update the course
        mSimpleCursorAdapter.changeCursor(mHelper.getAllContacts());
        mSimpleCursorAdapter.notifyDataSetChanged();
        clearText();
    }

    private void clearText() {
        mEditTextName.setText("");
        mEditTextPhone.setText("");
    }

    private void copyImageFromResources(int id, String filename)
    {
       Bitmap bitmap =  BitmapFactory.decodeResource(getResources(),id);
        try (FileOutputStream out = openFileOutput(filename,MODE_PRIVATE)) {

            if(openFileInput(filename) != null)
                return;
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

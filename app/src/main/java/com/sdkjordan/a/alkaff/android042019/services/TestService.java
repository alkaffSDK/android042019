package com.sdkjordan.a.alkaff.android042019.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.sdkjordan.a.alkaff.android042019.Constants;

import static com.sdkjordan.a.alkaff.android042019.Constants.APP_TAG;

public class TestService extends Service {

    // startService(intent)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO: do the task
        String data = intent.getStringExtra(Constants.EXTRA_DATA);
        Log.i(APP_TAG, String.format("Thread name: %-10s , Class : %-10s, Data:%-10s", Thread.currentThread().getName(), "TestService", data));

        stopSelf(startId); // or stopService();
        return Service.START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

package com.sdkjordan.a.alkaff.android042019.activities;

import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.sdkjordan.a.alkaff.android042019.R;
import com.sdkjordan.a.alkaff.android042019.fragments.MyDetailsFragment;
import com.sdkjordan.a.alkaff.android042019.fragments.MyListFragment;

public class DynamicFragmentActivity extends AppCompatActivity implements MyListFragment.ListFragmentInterface{

    private  String[] data ;
    private int lastID = -1 ;
    private MyDetailsFragment mDetailsFragment = new MyDetailsFragment() ;
    private FragmentManager fragmentManager ;

    private FrameLayout mListFrameLayout, mDetailsFrameLayout;

    private static final int MATCH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;
    private static final String TAG = "DynamicFragmentActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data = getResources().getStringArray(R.array.countries);

        setContentView(R.layout.activity_dynamic_fragment);

        fragmentManager = getSupportFragmentManager();

        mListFrameLayout =  findViewById(R.id.list_fragment_container);
        mDetailsFrameLayout =  findViewById(R.id.details_fragment_container);


        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.add(R.id.list_fragment_container,
                new MyListFragment());
        fragmentTransaction.commit();

        fragmentManager
                .addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
                    public void onBackStackChanged() {
                        setLayout();
                    }
                });
    }

    private void setLayout() {
        if (!mDetailsFragment.isAdded()) {
            mListFrameLayout.setLayoutParams(new LinearLayout.LayoutParams(
                    MATCH_PARENT, MATCH_PARENT));
            mDetailsFrameLayout.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT,
                    0));
        } else {
            mListFrameLayout.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT,
                    0, 2f));
            mDetailsFrameLayout.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT,
                    0, 1f));
        }
    }

    @Override
    public String[] getData() {return  data ;}

    @Override
    public void selectChanged(int id) {

        if (!mDetailsFragment.isAdded()) {
            FragmentTransaction fragmentTransaction = fragmentManager
                    .beginTransaction();
            fragmentTransaction.add(R.id.details_fragment_container,
                    mDetailsFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            fragmentManager.executePendingTransactions();
        }

        if(mDetailsFragment != null && lastID != id)
        {
            String[] split = data[id].split(":");
            mDetailsFragment.changeContent(id,split[0], split.length >= 2 ? split[1] : null) ;

            lastID = id ;
        }

    }
}
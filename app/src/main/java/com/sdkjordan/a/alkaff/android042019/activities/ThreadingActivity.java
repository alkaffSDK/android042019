package com.sdkjordan.a.alkaff.android042019.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.sdkjordan.a.alkaff.android042019.R;
import com.sdkjordan.a.alkaff.android042019.helpers.SQLHelper;

import java.io.File;
import java.net.URL;

public class ThreadingActivity extends AppCompatActivity {

    private static final long DELAY = 3000;
    private static final long PERIOD = DELAY / 100;
    private ImageView mImageView;
    private ProgressBar mProgressBar;

    private boolean mIsLoadingFlag = false;

    private SQLHelper sqlHelper ;

    private static boolean IsLoadingFlag = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_threading);

        sqlHelper = new SQLHelper(getApplicationContext());

        mImageView = findViewById(R.id.imageView2);
        mProgressBar = findViewById(R.id.progressBar);
        mProgressBar.setMax(100);
    }

    private ImageLoader mImageLoader ;
    private ImageLoader1 mImageLoader1 ;
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonLoad:
                mProgressBar.setProgress(0);
                //loadImageWithoutThreading(mImageView);
                //loadImageWithThreading(mImageView);
//                if(mImageLoader == null || (mImageLoader != null && mImageLoader.getStatus().equals(AsyncTask.Status.FINISHED))) {
//                    mImageLoader = new ImageLoader(getApplication(), mImageView, mProgressBar);
//                    mImageLoader.execute(R.raw.android3);       // call doInbackground
//
//                }else {
//                    return;
//                }

                if(mImageLoader1 == null || (mImageLoader1 != null && mImageLoader1.getStatus().equals(AsyncTask.Status.FINISHED))) {
                    mImageLoader1 = new ImageLoader1();
                    mImageLoader1.execute(R.raw.android3);       // call doInbackground

                }else {
                    return;
                }
                break;
            case R.id.buttonToast:
                Toast.makeText(getApplicationContext(), "Working ..", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void loadImageWithoutThreading(ImageView imageView) {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.raw.android3);
        waitForSomeTime();
        imageView.setImageBitmap(bitmap);
    }


    private void loadImageWithThreading(final ImageView imageView) {
        if (mIsLoadingFlag)
            return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                mIsLoadingFlag = true;
                final Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.raw.android3);
                waitForSomeTime();

                // the next line will throw an exception : Only the original thread that created a view hierarchy can touch its views.
                // imageView.setImageBitmap(bitmap);
                // To solve this error we have 2 options

                // Option 1: using View.post method
//                imageView.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        imageView.setImageBitmap(bitmap);
//                    }
//                });

                // Option 2: using  runOnUiThread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imageView.setImageBitmap(bitmap);
                    }
                });

                mProgressBar.setProgress(0);
                mIsLoadingFlag = false;
            }
        }).start();


    }

    private void waitForSomeTime() {
        try {
            for (int i = 1; i <= 100; i++) {
                Thread.sleep(PERIOD);
                mProgressBar.setProgress(i);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static class FileDownloader extends AsyncTask<URL,Integer, File>
    {

        @Override
        protected File doInBackground(URL... urls) {

            // TODO; download the file from urls and return the file
            return null;
        }
    }
                                                    // <Input,Progress,Output>
    private static class ImageLoader extends AsyncTask<Integer, Integer, Bitmap> {
        private ImageView imageView;
        private Context context;
        private ProgressBar progressBar;

        public ImageLoader(Context c, ImageView iv, @Nullable ProgressBar pb) {
            super();
            imageView = iv;
            context = c;
            progressBar = pb;

        }

        @Override
        protected void onPreExecute() {     // Runs before doInBackground on the main UI thread
            super.onPreExecute();
            if (progressBar != null) {
                progressBar.setMax(100);
                progressBar.setProgress(0);
            }
        }

        @Override
        protected Bitmap doInBackground(Integer... integers) {               // Run on the worker thread
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), integers[0]);
            try {
                for (int i = 1; i <= 100; i++) {
                    Thread.sleep(PERIOD);
                    publishProgress(i);      // will call onProgressUpdate
                }
                return bitmap;
            } catch (InterruptedException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {            // Runs after doInBackground on the main UI thread
            if (bitmap != null)
                imageView.setImageBitmap(bitmap);

            if (progressBar != null)
                progressBar.setProgress(0);
            super.onPostExecute(bitmap);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {            // Runs on the main UI thread
            progressBar.setProgress(values[0]);
        }

        @Override
        protected void onCancelled(Bitmap bitmap) {
            super.onCancelled(bitmap);
            Toast.makeText(context,"Task was cancelled",Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            Toast.makeText(context,"Task was cancelled",Toast.LENGTH_SHORT).show();
        }

    }

    // You can build it as non-static class, but not recommended
    private class ImageLoader1 extends AsyncTask<Integer, Integer, Bitmap> {

        @Override
        protected void onPreExecute() {     // Runs before doInBackground on the main UI thread
            super.onPreExecute();
            if (mProgressBar != null) {
                mProgressBar.setMax(100);
                mProgressBar.setProgress(0);
            }
        }

        @Override
        protected Bitmap doInBackground(Integer... integers) {               // Run on the worker thread
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), integers[0]);
            try {
                for (int i = 1; i <= 100; i++) {
                    Thread.sleep(PERIOD);
                    publishProgress(i);      // will call onProgressUpdate
                }
                return bitmap;
            } catch (InterruptedException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {            // Runs after doInBackground on the main UI thread
            if (bitmap != null)
                mImageView.setImageBitmap(bitmap);

            if (mProgressBar != null)
                mProgressBar.setProgress(0);
            super.onPostExecute(bitmap);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {            // Runs on the main UI thread
            mProgressBar.setProgress(values[0]);
        }

        @Override
        protected void onCancelled(Bitmap bitmap) {
            super.onCancelled(bitmap);
            Toast.makeText(getApplicationContext(),"Task was cancelled",Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            Toast.makeText(getApplicationContext(),"Task was cancelled",Toast.LENGTH_SHORT).show();
        }

    }

}

package com.sdkjordan.a.alkaff.android042019.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.sdkjordan.a.alkaff.android042019.Constants;

import static com.sdkjordan.a.alkaff.android042019.Constants.APP_TAG;

public class MyIntentService extends IntentService {

    private String ClassName = MyIntentService.this.getClass().getSimpleName();


    public MyIntentService() {
        super("A");
    }
    public MyIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String data = "" ;
        if(intent != null)
         data = intent.getStringExtra(Constants.EXTRA_DATA);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.i(APP_TAG, String.format("Thread name: %-10s , Class : %-10s, Data:%-10s", Thread.currentThread().getName(), ClassName, data));

    }


}

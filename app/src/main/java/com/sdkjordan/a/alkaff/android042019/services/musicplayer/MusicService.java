package com.sdkjordan.a.alkaff.android042019.services.musicplayer;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.sdkjordan.a.alkaff.android042019.R;

import java.util.ArrayList;
import java.util.Random;

public class MusicService extends Service implements
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnCompletionListener,
        MediaPlayer.OnErrorListener,
        AudioManager.OnAudioFocusChangeListener {

    private  final String TAG = this.getClass().getSimpleName();
    private static final int NOTIFY_ID = 2 ;
    private MediaPlayer player;
    private int songPosition = -1;
    private ArrayList<Song> songs;
    private String songTitle;
    private Song playSong;
    private boolean shuffle;
    private Random rand;
    private int currSongID;

    public boolean isShuffle() {
        return shuffle;
    }

    public void setShuffle(boolean shuffle) {
        if(shuffle) shuffle=false;
        else shuffle=true;
    }

    @Override
    public void onAudioFocusChange(int i) {

    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if(player.getCurrentPosition() > 0){
            mp.reset();
            playNext();
        }
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        mediaPlayer.reset();
        Log.e(TAG,"Error playing " + i1 ) ;
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        Log.i(TAG,"onPrepared " ) ;
        mediaPlayer.start();
        Intent notIntent = new Intent(this, PlayerActivity.class);
        notIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendInt = PendingIntent.getActivity(this, 0,
                notIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(this);

        builder.setContentIntent(pendInt)
                .setSmallIcon(R.mipmap.play)
                .setTicker(songTitle)
                .setOngoing(true)
                .setContentTitle("Playing")
        .setContentText(songTitle);
        Notification not = builder.build();

        startForeground(NOTIFY_ID, not);

    }


     class MusicBinder extends Binder {
        public MusicService getService() {
            return MusicService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        rand = new Random();

    }

    @Override
    public void onDestroy() {
        stopForeground(true);
    }

    public void setSongs(ArrayList<Song> songs) {
        this.songs = songs;
    }


    private final IBinder musicBind = new MusicBinder();


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        player.stop();
        player.release();
        return false;
    }

    public int getCurrentPosition() {
        return player.getCurrentPosition();
    }

    public int getDuration() {
        return player.getDuration();
    }

    public boolean isPlaying() {
        return player.isPlaying();
    }

    public void pausePlayer() {
        player.pause();
    }

    public void seek(int posn) {
        player.seekTo(posn);
    }

    public void start() {
        player.start();
    }

    public void setSong(int songIndex) {
        songPosition = songIndex;
        player = MediaPlayer.create(getApplicationContext(), songs.get(songIndex).getID());
    }

    public void playPrev() {
        songPosition--;
        if (songPosition < 0) songPosition = songs.size() - 1;
        playSong();
        songTitle = playSong.getTitle();
    }

    public void playSong() {

        //get song
        Song playSong = songs.get(songPosition);
        //get id
        int currSong = playSong.getID();

        songTitle=playSong.getTitle();

        if (currSongID != currSong) {
            player = MediaPlayer.create(this, currSong);
            Log.w(TAG, String.format("Track %s is prepared ", playSong.getTitle()));
            if (player == null)
                Log.e(TAG, "Error setting data source");

            currSongID = currSong;
        }

    }

    //skip to next
    public void playNext() {
        if (shuffle) {
            int newSong = songPosition;
            while (newSong == songPosition) {
                newSong = rand.nextInt(songs.size());
            }
            songPosition = newSong;
        } else {
            songPosition++;
            if (songPosition >= songs.size()) songPosition = 0;
        }
        playSong();
    }
}

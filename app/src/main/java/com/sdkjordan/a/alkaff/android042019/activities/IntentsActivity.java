package com.sdkjordan.a.alkaff.android042019.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.sdkjordan.a.alkaff.android042019.Constants;
import com.sdkjordan.a.alkaff.android042019.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class IntentsActivity extends AppCompatActivity {

    private static final String TAG = "android042019" ;
    private static  String ACTIVITY = "IntentsActivity" ;

    private static final int MY_REQUEST_CODE = 10 ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intents);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Activity a ;
        init();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }


    private EditText mEditTextData ;
    private void init() {
        mEditTextData = findViewById(R.id.editTextData1);

    }

    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.buttonEI:
                startActivityWithEI();
                break;
            case R.id.buttonII:
                startActivityWithII();
                break;
            case R.id.buttonView:
                startActivityWithActionView();
                break;
            case R.id.buttonViewViewPhone:
                startActivityWithActionViewForPhone(mEditTextData.getText().toString());
                break;
            case R.id.buttonViewViewSite:
                startActivityWithActionViewForSite(mEditTextData.getText().toString());
                break;
            case R.id.buttonStartForResult :
                startActivitToGetResult();
        }
    }

    private void startActivitToGetResult() {
        Intent intent = new Intent(Constants.ACTIONS.MY_ACTION);
        intent.putExtra(Constants.EXTRA_DATA, mEditTextData.getText().toString());
        intent.putExtra(Constants.EXTRA_RESULT,20);
        startActivityForResult(intent,MY_REQUEST_CODE);
    }

    private void startActivityWithActionViewForSite(String site) {
        String url  = "";
        Intent intent = null;
        if(site.startsWith("http://"))
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url)) ;
        else
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + url)) ;

        startActivity(intent);
    }

    private void startActivityWithActionViewForPhone(String number) {

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tel:"+ number));
        startActivity(intent);
    }

    private void startActivityWithActionView() {
        Log.d(TAG,ACTIVITY + ": startActivityWithActionView");
        // Implicit Intent
        Intent a = new Intent(Intent.ACTION_VIEW);
        a.putExtra(Constants.EXTRA_DATA, mEditTextData.getText().toString());
        a.putExtra(Constants.EXTRA_RESULT,10);
        startActivity(a);
    }


    private void startActivityWithII() {

        Log.d(TAG,ACTIVITY + ": startActivityWithII");
        // Implicit Intent
        Intent intent = new Intent(Constants.ACTIONS.MY_ACTION);
        intent.putExtra(Constants.EXTRA_DATA, mEditTextData.getText().toString());
        intent.putExtra(Constants.EXTRA_RESULT,10);
        startActivity(intent);

    }

    private void startActivityWithEI() {
        Log.d(TAG,ACTIVITY + ": startActivityWithEI");
        // Explicit Intent
        Intent intent = new Intent(IntentsActivity.this, MyNewActivity.class);
        intent.putExtra(Constants.EXTRA_DATA,mEditTextData.getText().toString());
        intent.putExtra(Constants.EXTRA_RESULT,50);
        startActivity(intent);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {

        if(requestCode == MY_REQUEST_CODE)
        {
            switch (resultCode)
            {
                case RESULT_OK:
                 String res = intent.getStringExtra(Constants.EXTRA_RESULT);
                    mEditTextData.setText(res);
                    break;
                case RESULT_CANCELED:
                    mEditTextData.setText("");
                    mEditTextData.setHint("No data found!");
                    break;
                case RESULT_FIRST_USER:

                    break;
            }
        }

    }
}

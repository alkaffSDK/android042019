package com.sdkjordan.a.alkaff.android042019.services.musicplayer;

public class Song {
    private int id;
    private String title;
    private String artist;

    public Song(int id, String title, String artist) {
        this.id = id;
        this.title = title;
        this.artist = artist;
    }


    public int getID(){return id;}
    public String getTitle(){return title;}
    public String getArtist(){return artist;}
}

package com.sdkjordan.a.alkaff.android042019.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sdkjordan.a.alkaff.android042019.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class MyCustomAdapter extends BaseAdapter {

    private static final String KEY_NAME = "name";
    private static final String KEY_PHONE = "phone";
    private static final String KEY_IMAGE = "img";
    private Context mContext;
    private List<Map<String, String>> mData = new ArrayList<>();

    private Random r = new Random();

    public MyCustomAdapter(Context context) {
        mContext = context;
    }

    public boolean add(String name, String phone, boolean autoNotify) {
        if (name != null && !name.isEmpty() && phone != null && !phone.isEmpty()) {
            HashMap<String, String> map = new HashMap<>();
            map.put(KEY_NAME, name);
            map.put(KEY_PHONE, phone);
            map.put(KEY_IMAGE, getRandomId());
            mData.add(map);
            if(autoNotify)
                notifyDataSetChanged();
            return true;
        }
        return false;
    }

    private String getRandomId() {
        switch (r.nextInt(3)) {
            case 0:
                return String.valueOf(R.mipmap.ic_play);
            case 1:
                return String.valueOf(R.mipmap.ic_pause);
            case 2:
                return String.valueOf(R.mipmap.ic_stop);
            default:
                return String.valueOf(R.mipmap.ic_launcher);
        }
    }

    @Override
    public int getCount() {
        if (mData == null)
            return 0;
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        if (mData == null)
            return null;

        if (position >= 0 && position < mData.size())
            return mData.get(position);

        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(mContext);

        View v = inflater.inflate(R.layout.list_item3, null);
        TextView textView1 = v.findViewById(R.id.textViewName);
        TextView textView2 = v.findViewById(R.id.textViewMobile);
        ImageView imageView = v.findViewById(R.id.imageView);

        textView1.setText(mData.get(position).get(KEY_NAME));
        textView2.setText(mData.get(position).get(KEY_PHONE));
        imageView.setImageResource(Integer.valueOf(mData.get(position).get(KEY_IMAGE)));

        return v;
    }
}

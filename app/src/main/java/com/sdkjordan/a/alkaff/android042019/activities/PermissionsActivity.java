package com.sdkjordan.a.alkaff.android042019.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.sdkjordan.a.alkaff.android042019.R;
import com.sdkjordan.a.alkaff.android042019.helpers.SQLiteHelper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.ArrayList;

public class PermissionsActivity extends AppCompatActivity implements View.OnClickListener {


    private static final int MY_CALL_PERMISSION_RQT = 18;
    private ImageButton mImageButtonCall;
    private EditText mEditTextPhone;
    private String  phone;
    private FloatingActionButton floatingActionButton;
    private String[] mPermissions = {Manifest.permission.CALL_PHONE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private ListView mPhonesListView ;
    private SimpleCursorAdapter simpleCursorAdapter ;

    SQLiteHelper mHelper ;
    private String[] from = {SQLiteHelper.COLUMN_PHONE};
    private int[] to = {R.id.text};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mHelper = new SQLiteHelper(getApplicationContext());

        mPhonesListView = findViewById(R.id.phones_list);
        simpleCursorAdapter = new SimpleCursorAdapter(getApplicationContext(),R.layout.my_list_item,LoadDate(),from,to,SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        mPhonesListView.setAdapter(simpleCursorAdapter);


        floatingActionButton = findViewById(R.id.fab);

        mImageButtonCall = findViewById(R.id.imageButtonCall);
        mEditTextPhone = findViewById(R.id.editTextPhone);

        mImageButtonCall.setOnClickListener(this);


    }

    private Cursor LoadDate() {
        return mHelper.getAllPhones();
    }

    @Override
    public void onClick(View v) {
        phone = mEditTextPhone.getText().toString();
        mEditTextPhone.setText("");
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if(ActivityCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
            {
                if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE))
                {
                    Toast.makeText(this,"We need this permission to make the call.",Toast.LENGTH_LONG).show();
                }

                requestPermissions(new String[] {Manifest.permission.CALL_PHONE}, MY_CALL_PERMISSION_RQT);
                return;
            }else{
                call(phone);
            }
        }else {         // API is less than 23
            call(phone);


        }
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            ArrayList<String> requestedPermission = new ArrayList<>();
//            for (String permission : mPermissions) {
//                if (ActivityCompat.checkSelfPermission(getApplicationContext(), permission) != PackageManager.PERMISSION_GRANTED) {
//                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
//                        // this permission was requested before and denied , you show explain why this permission is needed
//                        Toast.makeText(this, String.format("We need this permission \"%s\" to do the action \"%s\"", permission, "Action"), Toast.LENGTH_LONG).show();
//                    }
//                    requestedPermission.add(permission);
//                }
//            }
//            if (requestedPermission.size() > 0) {
//                String[] permission = new String[requestedPermission.size()];
//                requestedPermission.toArray(permission);
//                requestPermissions(permission, MY_CALL_PERMISSION_RQT);
//            }
//            return;
////            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
////                // App has no permission
////
////                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) {
////                    // this permission was requested before and denied , you show explain why this permission is needed
////                    Toast.makeText(this, "We need this permission to make the phone call", Toast.LENGTH_LONG).show();
////                }
////
////
////                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
////                    // this permission was requested before and denied , you show explain why this permission is needed
////                    Toast.makeText(this, "We don't need this permission to make the phone call, just for testing", Toast.LENGTH_LONG).show();
////                }
//
//
////            }
//        }
//        startActivity(intent);
    }

    private void call(String phone)
    {
        mHelper.InsertPhone(phone);
        simpleCursorAdapter.changeCursor(LoadDate());
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_CALL_PERMISSION_RQT) {
            for (int i = 0; i < grantResults.length; i++)
                if (grantResults[i] == PackageManager.PERMISSION_DENIED)
                    return;
            call(phone);
        }
    }
}

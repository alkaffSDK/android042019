package com.sdkjordan.a.alkaff.android042019.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.security.PrivateKey;


public class SQLiteHelper extends SQLiteOpenHelper {

    public static final String DATA_BASE_NAME = "my_db";
    public static final String TABLE_NAME = "Phones";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_PHONE = "PhoneNumber";

    SQLiteDatabase mSqLiteDatabase ;

    public static final int VERSION = 1;
    private static final String CREATE_NUMBERS_TABLE = String.format("CREATE TABLE %s  ( %s INTEGER   PRIMARY KEY AUTOINCREMENT, %s TEST NOT NULL);",
            TABLE_NAME, COLUMN_ID, COLUMN_PHONE);

    public SQLiteHelper(@Nullable Context context) {
        super(context, DATA_BASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_NUMBERS_TABLE);

        mSqLiteDatabase = db ;
    }

    public long InsertPhone(String phone) {

        mSqLiteDatabase = getWritableDatabase() ;

        ContentValues values = new ContentValues();
        values.put(COLUMN_PHONE,qutoed(phone));
        return  mSqLiteDatabase.insert(TABLE_NAME,null,values);
    }

    private String qutoed(String phone) {
        return  ( phone);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO: Do data base migration
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }

    public Cursor getAllPhones() {
        return getReadableDatabase().query(TABLE_NAME,null,null,null,null,null,null) ;
    }
}

package com.sdkjordan.a.alkaff.android042019.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.util.Log;

import com.sdkjordan.a.alkaff.android042019.R;
import com.sdkjordan.a.alkaff.android042019.fragments.MyDetailsFragment;
import com.sdkjordan.a.alkaff.android042019.fragments.MyListFragment;

public class StaticFragmentActivity extends AppCompatActivity implements MyListFragment.ListFragmentInterface {

    private  String[] data ;
    private int lastID = -1 ;
    private MyDetailsFragment myDetailsFragment ;
    private FragmentManager fragmentManager ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data = getResources().getStringArray(R.array.countries);
        setContentView(R.layout.activity_static_fragement);

        fragmentManager = getSupportFragmentManager();

        try {
            myDetailsFragment = (MyDetailsFragment) fragmentManager.findFragmentById(R.id.fragment_details);
        }catch (Exception ex)
        {
            Log.getStackTraceString(ex);
        }
    }

    @Override
    public String[] getData() {
        lastID = -1 ;
        return  data ;}

    @Override
    public void selectChanged(int position) {

        if(myDetailsFragment != null && lastID != position)
        {
            String[] split = data[position].split(":");
            if(split.length >= 2)
                myDetailsFragment.changeContent(position,split[0],split[1]) ;
            else
                myDetailsFragment.changeContent(position,split[0],null) ;
            lastID = position ;
        }

    }
}

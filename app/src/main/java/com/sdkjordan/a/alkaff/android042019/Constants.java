package com.sdkjordan.a.alkaff.android042019;

public class Constants {

    public static final String APP_TAG = "android042019";
    public static final String EXTRA_DATA = "data";
    public static final String EXTRA_RESULT = "result";
    public static final String SHARED_PREFRENCES_FILE = "MyPrefs";

    public  static final class ACTIONS {
        public static final String MY_ACTION = "com.sdkjordan.a.alkaff.android042019.MY_ACTION";
    }


}

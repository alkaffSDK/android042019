package com.sdkjordan.a.alkaff.android042019.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.sdkjordan.a.alkaff.android042019.Constants;

public class DynamicReceiver extends BroadcastReceiver {
    private String TAG = "Receiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        String data = "No data found!";
        if(intent != null && intent.getStringExtra(Constants.EXTRA_DATA) != null )
        {
            data = intent.getStringExtra(Constants.EXTRA_DATA);
        }
        Log.d(TAG,"Dynamic Receiver : Data:"+ data);
    }

    public  static IntentFilter getIntentFilter()
    {
        return new IntentFilter(Constants.ACTIONS.MY_ACTION);
    }
}

package com.sdkjordan.a.alkaff.android042019.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.sdkjordan.a.alkaff.android042019.R;
import com.sdkjordan.a.alkaff.android042019.fragments.MyDetailsFragment;
import com.sdkjordan.a.alkaff.android042019.fragments.MyListFragment;

public class ProgrammaticFragmentActivity extends AppCompatActivity implements MyListFragment.ListFragmentInterface{

    private  String[] data ;
    private int lastID = -1 ;
    private MyDetailsFragment mDetailsFragment = new MyDetailsFragment() ;
    private FragmentManager fragmentManager ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data = getResources().getStringArray(R.array.countries);
        setContentView(R.layout.activity_programmatic_fragment);

        fragmentManager = getSupportFragmentManager();


        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.list_frame, mDetailsFragment );
        transaction.add(R.id.details_frame, new MyListFragment());
        transaction.commit();
    }


    @Override
    public String[] getData() {return  data ;}

    @Override
    public void selectChanged(int id) {

        if(mDetailsFragment != null && lastID != id)
        {
            String[] split = data[id].split(":");
            mDetailsFragment.changeContent(id,split[0],split[1] ) ;
            lastID = id ;
        }

    }
}

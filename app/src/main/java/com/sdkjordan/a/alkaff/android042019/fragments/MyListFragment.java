package com.sdkjordan.a.alkaff.android042019.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import com.sdkjordan.a.alkaff.android042019.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyListFragment extends ListFragment {

    public interface ListFragmentInterface {
        String[] getData();
        void selectChanged(int id);
    }

    private static final String TAG = "Fragment" ;
    private static final String CLASS_NAME = "MyListFragment" ;

    Context mContext ;
    ListFragmentInterface mActivity ;


    private String[] mDataList = {""} ;
    private ArrayAdapter<String> mAdapter ;


    public MyListFragment() {

    }

    public MyListFragment(String[] list) {
        setDataList(list);
    }

    public void setDataList(String[] list)
    {
        mDataList =  list ;
        if(mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onListItemClick(@NonNull ListView l, @NonNull View v, int position, long id) {
        mActivity.selectChanged(position);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context ;
        Log.d(TAG,String.format("%-15s ->%s",CLASS_NAME,"onAttach()"));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,String.format("%-15s ->%s",CLASS_NAME,"onCreate()"));

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG,String.format("%-15s ->%s",CLASS_NAME,"onCreateView()"));
        return super.onCreateView(inflater,container,savedInstanceState) ;
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.d(TAG,String.format("%-15s ->%s",CLASS_NAME,"onActivityCreated()"));
        super.onActivityCreated(savedInstanceState);
        // Activity is created
        try {
            mActivity = (ListFragmentInterface) getActivity();
            setDataList(mActivity.getData());
            mAdapter = new ArrayAdapter<>(mContext,R.layout.my_list_item,R.id.text,mDataList);
            setListAdapter(mAdapter);           // or  getListView().setAdapter(mAdapter);

           // mAdapter.notifyDataSetChanged();


        }catch (Exception ex)
        {
            Log.getStackTraceString(ex);
        }


    }

    @Override
    public void onStart() {
        Log.d(TAG,String.format("%-15s ->%s",CLASS_NAME,"onStart()"));
        super.onStart();

    }

    @Override
    public void onResume() {
        Log.d(TAG,String.format("%-15s ->%s",CLASS_NAME,"onResume()"));
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d(TAG,String.format("%-15s ->%s",CLASS_NAME,"onPause()"));
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d(TAG,String.format("%-15s ->%s",CLASS_NAME,"onStop()"));
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG,String.format("%-15s ->%s",CLASS_NAME,"onDestroyView()"));
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG,String.format("%-15s ->%s",CLASS_NAME,"onDestroy()"));
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Log.d(TAG,String.format("%-15s ->%s",CLASS_NAME,"onDetach()"));
        super.onDetach();
    }
}

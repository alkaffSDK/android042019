package com.sdkjordan.a.alkaff.android042019.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sdkjordan.a.alkaff.android042019.R;
import com.sdkjordan.a.alkaff.android042019.helpers.CheckForSDCard;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

public class DownloadFileActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    private static final int WRITE_REQUEST_CODE = 300;
    private static final String TAG = DownloadFileActivity.class.getSimpleName();
    private String url;
    private EditText editTextUrl;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_file);

        mContext = getApplicationContext();
        editTextUrl = findViewById(R.id.editTextUrl);

        Button downloadButton = findViewById(R.id.buttonDownload);
        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Check if SD card is present or not
                if (CheckForSDCard.isSDCardPresent()) {

                    //check if app has permission to write to the external storage.
                    if (EasyPermissions.hasPermissions(DownloadFileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        //Get the URL entered
                        url = editTextUrl.getText().toString();
                        new DownloadFile(DownloadFileActivity.this).execute(url);

                    } else {
                        //If permission is not present request for the same.
                        EasyPermissions.requestPermissions(DownloadFileActivity.this, getString(R.string.write_file), WRITE_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
                    }


                } else {
                    Toast.makeText(getApplicationContext(),
                            "SD Card not found", Toast.LENGTH_LONG).show();

                }
            }

        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, DownloadFileActivity.this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        //Download the file once permission is granted
        url = editTextUrl.getText().toString();
        new DownloadFile(this).execute(url);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(TAG, "Permission has been denied");
    }

    /**
     * Async Task to download file from URL
     */
    private static class DownloadFile extends AsyncTask<String, Integer, String> {

        private ProgressDialog progressDialog;
        private String fileName;
        private String folder;
        private boolean isDownloaded;
        private Context mContext;
        private PowerManager.WakeLock mWakeLock;

        public DownloadFile(Context mContext) {
            this.mContext = mContext;
        }

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) mContext.getSystemService(mContext.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();


            this.progressDialog = new ProgressDialog(mContext);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            InputStream input = null;
            OutputStream output = null;
            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                // This will disable "keep-alive" property which is on by default. to be able to download large files
                connection.setRequestProperty("connection", "close");
                connection.connect();
                // getting file length
                int lengthOfFile = -1 ;
                for (int i = 0;lengthOfFile <0 &&  i < 10 ; i++) {
                    lengthOfFile = connection.getContentLength();
                }

                if(lengthOfFile == -1)
                    lengthOfFile = 8192  ;



                // input stream to read file - with 8k buffer


                input = new BufferedInputStream(url.openStream());
                String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
                //Extract file name from URL
                fileName = f_url[0].substring(f_url[0].lastIndexOf('/') + 1, f_url[0].length());

                //Append timestamp to file name
                fileName = timestamp + "_" + fileName;

                //External directory path to save file
                folder = Environment.getExternalStorageDirectory() + File.separator + "androiddeft/";


                //Create androiddeft folder if it does not exist
                File directory = new File(folder);

                if (!directory.exists()) {
                    directory.mkdirs();
                }

                // Output stream to write file
                output = new FileOutputStream(folder + fileName);

                byte data[] = new byte[8192];

                long total = 0;
                int progress = 0, lastProgress = 0 ;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    progress = (int) ((total * 100) / lengthOfFile);
                    if(lastProgress != progress) {
                        publishProgress(progress);
                        Log.d(TAG, "Progress: " + progress);
                        lastProgress = progress ;
                    }

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();


            } catch (Exception e) {
                Log.e(TAG, "Error: " + e.getMessage());
                return "Something went wrong" + e.getStackTrace();
            } finally {

                try {
                    if (output != null) output.close();

                    if (input != null) input.close();

                } catch (IOException e) {
                    e.printStackTrace();

                    return "Error closing" + e.getStackTrace();
                }

                return "Downloaded at: " + folder + fileName;

            }


        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(Integer... progress) {
            // setting progress percentage
            progressDialog.setProgress(progress[0]);
        }


        @Override
        protected void onPostExecute(String message) {

            mWakeLock.release();
            // dismiss the dialog after the file was downloaded
            this.progressDialog.dismiss();

            // Display File path after downloading
            Toast.makeText(mContext.getApplicationContext(),
                    message, Toast.LENGTH_LONG).show();
        }
    }
}

package com.sdkjordan.a.alkaff.android042019.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.sdkjordan.a.alkaff.android042019.R;

public class EventsActivity extends
        AppCompatActivity implements
        View.OnClickListener,
        View.OnLongClickListener,
        View.OnFocusChangeListener {

    private Button mButtonMethod2;
    private Button mButtonMethod3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);

        mButtonMethod2 = findViewById(R.id.buttonMethod2);
        mButtonMethod3 = findViewById(R.id.buttonMethod3);

        // Method 2: by create an event listener for each view
        // Advantages : can be used with all listeners
        // Disadvantage : create an object for each view
//        mButtonMethod2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //TODO: Do you onclick action here
//
//                switch (v.getId()) {
//                    case R.id.buttonMethod2:
//                        //TODO: Do you onclick action for button 1
//                        break;
//                    case R.id.buttonMethod3:
//                        //TODO: Do you onclick action here  for button 2
//                        break;
//                }
//            }
//        });

//        mButtonMethod2.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                switch (v.getId()) {
//                    case R.id.buttonMethod2:
//                        //TODO: Do you onclick action for button 1
//                        break;
//                    case R.id.buttonMethod3:
//                        //TODO: Do you onclick action here  for button 2
//                        break;
//                }
//                return false;
//            }
//        });


        // or // Method 3: by create one event listener for all views
        // Advantages : can be used with all listeners
        //  Advantages : create only one object for each listener

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: Do you onclick action here

                switch (v.getId()) {
                    case R.id.buttonMethod2:
                        //TODO: Do you onclick action for button 1
                        break;
                    case R.id.buttonMethod3:
                        //TODO: Do you onclick action here  for button 2
                        break;
                }
            }
        };

        View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                switch (v.getId()) {
                    case R.id.buttonMethod2:
                        //TODO: Do you onclick action for button 1
                        break;
                    case R.id.buttonMethod3:
                        //TODO: Do you onclick action here  for button 2
                        break;
                }
                return false;
            }
        };

//        mButtonMethod2.setOnClickListener(clickListener);
//        mButtonMethod3.setOnClickListener(clickListener);
//
//        mButtonMethod2.setOnLongClickListener(onLongClickListener);
//        mButtonMethod3.setOnLongClickListener(onLongClickListener);


        // or // Method 4: by using the activity as a listener  all views ()
        //  Advantages  : Can be used with all listeners
        //  Advantages  : Create no more object for each listener, because it uses the activity object a listener
        //  Advantages  : Clean code

        mButtonMethod2.setOnClickListener(this);
        mButtonMethod3.setOnClickListener(this);

        mButtonMethod2.setOnLongClickListener(this);
        mButtonMethod3.setOnLongClickListener(this);

        mButtonMethod2.setOnFocusChangeListener(this);
    }

    public void doAction(View view) {
        //TODO: Do you onclick action here
    }

    @Override
    public void onClick(View v) {
        //TODO: Do you onclick action here
        switch (v.getId()) {
            case R.id.buttonMethod2:
                //TODO: Do you onclick action for button 1
                break;
            case R.id.buttonMethod3:
                //TODO: Do you onclick action here  for button 2
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.buttonMethod2:
                //TODO: Do you onclick action for button 1
                break;
            case R.id.buttonMethod3:
                //TODO: Do you onclick action here  for button 2
                break;
        }
        return true;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus)
            v.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        else
            v.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
    }
}


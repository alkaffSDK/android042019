package com.sdkjordan.a.alkaff.android042019.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.sdkjordan.a.alkaff.android042019.R;

public class FragmentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
    }

    public void onClick(View view) {
        Class clz = null ;
        switch (view.getId())
        {
            case R.id.buttonStatic:
                clz = StaticFragmentActivity.class ; break;
            case R.id.buttonProg:
                clz = ProgrammaticFragmentActivity.class ; break;
            case R.id.buttonDynamic:
                clz = DynamicFragmentActivity.class ; break;

        }

       Intent intent = new Intent(this,clz);
        startActivity(intent);
    }

}

package com.sdkjordan.a.alkaff.android042019.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.sdkjordan.a.alkaff.android042019.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyListActivity extends AppCompatActivity {



    private String[] data = {"Jordan","Syria","Egypt","D","E"} ;

    private  String[] countries ;
    ListView mListView ;
    private String[] from = {"country","code"};
    private int[] to = {R.id.textCountry, R.id.textCode};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_list);

        mListView = findViewById(R.id.my_list_view);
//        ArrayAdapter<String> mSimpleArrayAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,data);


        countries = getResources().getStringArray(R.array.countries) ;
//        ArrayAdapter<String> mAdapter = new ArrayAdapter(getApplicationContext(),R.layout.my_list_item,countries);
//        ArrayAdapter<String> mAdapter = new ArrayAdapter(getApplicationContext(),R.layout.complex_item_list,R.id.text, countries);

        List<HashMap<String,String>> data =  splitData(countries);


        SimpleAdapter mAdapter = new SimpleAdapter(getApplicationContext(),data,R.layout.complex_item_list,from,to);

        mListView.setAdapter(mAdapter);
    }


    private List<HashMap<String,String>> splitData(String[] countries)
    {
        List<HashMap<String,String>> list = new ArrayList<>();

        String[] splited ;
         HashMap<String,String> map = null ;
        for (String c : countries)
        {
            splited = c.split(":") ;
             map = new HashMap<>();
             map.put("country",splited[0]);
             if(splited.length > 1)
                map.put("code",splited[1]);
             else
                 map.put("code","000");

            list.add(map);

        }

        return  list ;

    }
}

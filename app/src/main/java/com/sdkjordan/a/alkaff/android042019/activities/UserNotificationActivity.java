package com.sdkjordan.a.alkaff.android042019.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.sdkjordan.a.alkaff.android042019.Constants;
import com.sdkjordan.a.alkaff.android042019.R;

public class UserNotificationActivity extends AppCompatActivity implements View.OnClickListener, AlertDialog.OnClickListener {

    private static final String CHANNEL1_ID = "channel_1";
    private static final String CHANNEL2_ID = "channel_2";


    private EditText mEditTextData;

    private String[] countries;

    private AlertDialog mExitAlertDialog;
    private AlertDialog mDataAlertDialog;
    private AlertDialog mSingleChoiceDialog;
    private AlertDialog mMultiChoiceDialog;

    private static final int MY_PENNDING_REQUEST = 10;

    private boolean isBackpressed = false;


    Notification notification;
    NotificationManager notificationManager;

    private PendingIntent mpendingItent;
    private int mNotificationId = 1;
    private int mCheckedItem = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_notification);

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        createNotificationChannel();

        Intent intent = new Intent(Constants.ACTIONS.MY_ACTION);
        mpendingItent = PendingIntent.getActivity(getApplicationContext(), MY_PENNDING_REQUEST, intent, PendingIntent.FLAG_UPDATE_CURRENT);


        inti();
    }

    private void inti() {
        countries = getResources().getStringArray(R.array.countries);
        checkedItems = new boolean[countries.length];
        mEditTextData = findViewById(R.id.editTextData1);
    }

    public void onClick(View view) {


        String data = mEditTextData.getText().toString();
        mEditTextData.setText("");
//        if (data.isEmpty())
//            return;

        switch (view.getId()) {
            case R.id.buttonToast:
                showToast(data);
                break;
            case R.id.buttonCustomToast:
                showCustomToast(data);
                break;
            case R.id.buttonInputDialog:
                getDataFromDialog();
                break;
            case R.id.buttonSingleChoiceDialog:
                showSingleChoiceDialog();
                break;
            case R.id.buttonMultiDialog:
                showMultiChoiceDialog();
                break;
            case R.id.buttonPopUp:
                createPopUpMenu(view);
                break;

            case R.id.buttonNotification:
                if (notification == null) {
                    notification = createNotification(data);
                } else {
                    updateNotification(data);
                }
                notificationManager.notify(mNotificationId, notification);
                break;
        }
    }

    EditText editTextInputData;

    private void getDataFromDialog() {
        if (mDataAlertDialog == null) {

            View v = getLayoutInflater().inflate(R.layout.dialog_input_layout, null);
            editTextInputData = v.findViewById(R.id.editTextInputData);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false)
                    .setTitle(R.string.txt_data)
                    .setView(v)
                    .setPositiveButton(R.string.txt_ok, this)
                    .setNegativeButton(R.string.txt_cancel, this);
            mDataAlertDialog = builder.create();
        }

        mDataAlertDialog.show();
    }


    private void showToast(String msg) {
        Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private void showCustomToast(String msg) {
        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);

        // Inflate the toast layout
        View view = getLayoutInflater().inflate(R.layout.toast_layout, null);
        // get the text view from the toast view
        TextView textView = view.findViewById(R.id.textViewData);
        textView.setText(msg);
        toast.setView(view);
        toast.show();
    }


    private void showExitConfirmationDialog() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle(getResources().getString(R.string.txt_exit));
//        builder.setMessage(getResources().getString(R.string.txt_exit_msg));
//        builder.setPositiveButton(R.string.txt_ok, this);
//        builder.setNegativeButton(R.string.txt_cancel, this);
//        builder.setCancelable(true);
//        builder.show() ;
        if (mExitAlertDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getResources().getString(R.string.txt_exit))
                    .setMessage(getResources().getString(R.string.txt_exit_msg))
                    .setPositiveButton(R.string.txt_ok, this)
                    .setNegativeButton(R.string.txt_cancel, this)
                    .setCancelable(true);

            mExitAlertDialog = builder.create();
        }

        mExitAlertDialog.show();
    }


    int selectedItem = -1;

    private void showSingleChoiceDialog() {

        if (mSingleChoiceDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.txt_single_choice)
//                   .setMultiChoiceItems(countries, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
//                       @Override
//                       public void onClick(DialogInterface dialog, int which, boolean isChecked) {
//
//                       }
//                   })
                    .setSingleChoiceItems(countries, selectedItem, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            selectedItem = which;
                        }
                    })
                    .setPositiveButton(R.string.txt_ok, this)
                    .setNegativeButton(R.string.txt_cancel, this)
                    .setCancelable(false);

            mSingleChoiceDialog = builder.create();

        }
        mSingleChoiceDialog.show();
    }


    private boolean[] checkedItems;

    private void showMultiChoiceDialog() {

        if (mMultiChoiceDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.txt_single_choice)
                    .setMultiChoiceItems(countries, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                            checkedItems[which] = isChecked;
                        }
                    })
                    .setPositiveButton(R.string.txt_ok, this)
                    .setNegativeButton(R.string.txt_cancel, this)
                    .setCancelable(false);

            mMultiChoiceDialog = builder.create();

        }
        mMultiChoiceDialog.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

        switch (which) {
            case AlertDialog.BUTTON_POSITIVE:
                if (dialog.equals(mExitAlertDialog))
                    super.onBackPressed();          // or finish();
                else if (dialog.equals(mDataAlertDialog)) {
                    mEditTextData.setText(editTextInputData.getText());
                    editTextInputData.setText("");
                } else if (dialog.equals(mSingleChoiceDialog)) {
                    if (selectedItem > -1)
                        mEditTextData.setText(countries[selectedItem]);
                } else if (dialog.equals(mMultiChoiceDialog)) {
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < checkedItems.length; i++) {
                        if (checkedItems[i]) {
                            builder.append(countries[i] + " /");
                        }
                    }
                    mEditTextData.setText(builder.toString());
                }
                break;
            case AlertDialog.BUTTON_NEGATIVE:
                dialog.dismiss();
                break;
            case AlertDialog.BUTTON_NEUTRAL:
                break;
        }
    }


    private void createPopUpMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.popup_items, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        popup.show();
    }

    @Override
    public void onBackPressed() {
        showExitConfirmationDialog();

    }

    private void showSnackBar(@NonNull View v, @NonNull String text, @Nullable String action, @Nullable View.OnClickListener listener) {
        Snackbar mySnackbar = Snackbar.make(v, text, Snackbar.LENGTH_SHORT);
        if (action != null)
            mySnackbar.setAction(action, listener);
        mySnackbar.show();

    }

    NotificationCompat.Builder builder;

    private Notification createNotification(String text) {
        if (builder == null) {
            builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL1_ID);
            builder.setSmallIcon(android.R.drawable.ic_dialog_alert)
                    .setContentTitle("Notification")
                    .setContentIntent(mpendingItent)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentText(text);
        }

        return builder.build();
    }

    private void updateNotification(String text) {
        if (builder == null)
            notification = createNotification(text);

        else {
            builder.setContentText(text);
            notification = builder.build();
        }
    }


    private void createNotificationChannel() {

        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (null == notificationManager.getNotificationChannel(CHANNEL1_ID)) {
                CharSequence name = getString(R.string.channel_name);
                String description = getString(R.string.channel_description);
                int importance = NotificationManager.IMPORTANCE_DEFAULT;
                NotificationChannel channel = new NotificationChannel(CHANNEL1_ID, name, importance);
                channel.setDescription(description);
                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this
                // notificationManager = getSystemService(NotificationManager.class);
                notificationManager.createNotificationChannel(channel);
            }

            if (null == notificationManager.getNotificationChannel(CHANNEL2_ID)) {
                CharSequence name = getString(R.string.channel2_name);
                String description = getString(R.string.channel2_description);
                int importance = NotificationManager.IMPORTANCE_DEFAULT;
                NotificationChannel channel = new NotificationChannel(CHANNEL2_ID, name, importance);
                channel.setDescription(description);
                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this
                // notificationManager = getSystemService(NotificationManager.class);
                notificationManager.createNotificationChannel(channel);
            }
        }
    }
}

package com.sdkjordan.a.alkaff.android042019.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.sdkjordan.a.alkaff.android042019.R;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "android042019" ;
    private static  String ACTIVITY = "MainActivity" ;

    TextView mTextView ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG,ACTIVITY + ": onCreate");
        mTextView = findViewById(R.id.textView1);

        String text = "Hi";
        if(savedInstanceState != null)
        {
            text = savedInstanceState.getString("data","hi");
        }

        mTextView.setText(text);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,ACTIVITY + ": onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG,ACTIVITY + ": onRestart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,ACTIVITY + ": onStop");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("data","Ahmed");
        super.onSaveInstanceState(outState);
        Log.d(TAG,ACTIVITY + ": onSaveInstanceState");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,ACTIVITY + ": onDestroy");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG,ACTIVITY + ": onBackPressed");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,ACTIVITY + ": onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,ACTIVITY + ": onResume");
    }


    public void onClick(View view) {
        Intent intent = new Intent(MainActivity.this,MyNewActivity.class) ;
        startActivity(intent);
    }


}

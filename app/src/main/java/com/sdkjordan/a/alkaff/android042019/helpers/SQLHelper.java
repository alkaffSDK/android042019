package com.sdkjordan.a.alkaff.android042019.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQuery;

import androidx.annotation.Nullable;

public class SQLHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "my_db";
    public static final int VERSION = 1;
    public static final String TABLE_NAME = "contacts";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PHONE = "phone";
    public static final String[] ALL_COLUMNS = {COLUMN_ID,COLUMN_NAME,COLUMN_PHONE};

    private static final String CREATE_NUMBERS_TABLE = String.format("CREATE TABLE %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEST NOT NULL ,  %s TEST NOT NULL UNIQUE);",
            TABLE_NAME, COLUMN_ID, COLUMN_NAME, COLUMN_PHONE);


    public SQLHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    public boolean insertContact(String name, String phone)
    {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, name);
        values.put(COLUMN_PHONE,phone);
        return  getWritableDatabase().insert(TABLE_NAME,null,values) != -1;
    }

    public String getFirstPhoneByName(String name)
    {
        String[] columns = {COLUMN_PHONE};
        String selection = String.format(" %s = ?",COLUMN_NAME);            // name = ?
        String[] sleArgs = {name};
        Cursor c = getReadableDatabase().query(TABLE_NAME,columns,selection,sleArgs,null,null,null,"TOP 1") ;
        if(c != null && c.moveToFirst())
            return  c.getString(1);
        else
            return  null ;
    }

    public Cursor getAllPhoneByName(String name)
    {
        String[] columns = {COLUMN_PHONE};
        String selection = String.format(" %s = ?",COLUMN_NAME);            // name = ?
        String[] sleArgs = {name};
        return getReadableDatabase().query(TABLE_NAME,columns,selection,sleArgs,null,null,null) ;

    }

    public Cursor getAllContacts()
    {
        return getReadableDatabase().query(TABLE_NAME,null,null,null,null,null,null) ;

    }

    public Cursor getContactsByID(int id)
    {
        String selection = String.format(" %s = ?",COLUMN_ID);            // name = ?
        String[] sleArgs = {String.valueOf(id)};
        return getReadableDatabase().query(TABLE_NAME,null,selection,sleArgs,null,null,null) ;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_NUMBERS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS "+ DATABASE_NAME);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }
}

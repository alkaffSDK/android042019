package com.sdkjordan.a.alkaff.android042019.services;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

public class TestIntentService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public TestIntentService(String name) {
        super(name);
    }

    public TestIntentService() {
        this("TestIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }
}

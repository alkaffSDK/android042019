package com.sdkjordan.a.alkaff.android042019.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.sdkjordan.a.alkaff.android042019.Constants;

import static com.sdkjordan.a.alkaff.android042019.Constants.APP_TAG;


public class MyServices extends Service {

    private String ClassName = MyServices.this.getClass().getSimpleName();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(APP_TAG,"MyServices is started");
//        try {
//            Thread.sleep(25000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        String data = intent.getStringExtra(Constants.EXTRA_DATA);
//
//        Log.i(APP_TAG, String.format("Thread name: %-10s , Class : %-10s, Data:%-10s", Thread.currentThread().getName(), ClassName, data));
        final Intent mIntent1 = intent;
        new Thread(new Runnable() {
            @Override
            public void run() {
                String data = mIntent1.getStringExtra(Constants.EXTRA_DATA);
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.i(APP_TAG, String.format("Thread name: %-10s , Class : %-10s, Data:%-10s", Thread.currentThread().getName(), ClassName, data));
            }
        }).start();


        return START_REDELIVER_INTENT;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

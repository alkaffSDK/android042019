package com.sdkjordan.a.alkaff.android042019.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.sdkjordan.a.alkaff.android042019.Constants;
import com.sdkjordan.a.alkaff.android042019.R;

public class MyNewActivity extends AppCompatActivity {

    private static final String TAG = "android042019" ;
    private static  String ACTIVITY = "MyNewActivity" ;

    String data ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG,ACTIVITY + ": onCreate");

        setContentView(R.layout.activity_mynew);
        inti();


        data  = getIntent().getStringExtra(Constants.EXTRA_DATA) ;
        int result  = getIntent().getIntExtra(Constants.EXTRA_RESULT, 0) ;
        if(null != data && ! data.isEmpty() )
            mEditText.setText(data + " : "+ result);
        else
            mEditText.setHint(getResources().getString(R.string.txt_noData));

    }

    private EditText mEditText ;

    private void inti() {
        mEditText = findViewById(R.id.editText);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,ACTIVITY + ": onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG,ACTIVITY + ": onRestart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,ACTIVITY + ": onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,ACTIVITY + ": onDestroy");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG,ACTIVITY + ": onBackPressed");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,ACTIVITY + ": onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,ACTIVITY + ": onResume");
    }

    public void onClick(View v)
    {

        if(data.isEmpty())
        {
            setResult(RESULT_CANCELED);
        }else{

            Intent intent = new Intent();
            intent.putExtra(Constants.EXTRA_RESULT,new StringBuffer(data).reverse().toString()) ;
            setResult(RESULT_OK,intent);
        }
        finish();
    }

}

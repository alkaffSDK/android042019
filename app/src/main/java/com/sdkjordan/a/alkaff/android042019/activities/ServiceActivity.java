package com.sdkjordan.a.alkaff.android042019.activities;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.EditText;

import com.sdkjordan.a.alkaff.android042019.Constants;
import com.sdkjordan.a.alkaff.android042019.R;
import com.sdkjordan.a.alkaff.android042019.services.MyIntentService;
import com.sdkjordan.a.alkaff.android042019.services.MyServices;

public class ServiceActivity extends AppCompatActivity {

    private EditText mEditText ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mEditText = findViewById(R.id.editText2);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ServiceActivity.this, MyIntentService.class);
                intent.putExtra(Constants.EXTRA_DATA,mEditText.getText().toString());
                startService(intent);
                mEditText.setText("");
            }
        });
    }

}

package com.sdkjordan.a.alkaff.android042019.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MyIntentService3 extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_START = "com.sdkjordan.a.alkaff.android042019.services.action.START";
    private static final String ACTION_STOP = "com.sdkjordan.a.alkaff.android042019.services.action.STOP";
    private static final String ACTION_PAUSE = "com.sdkjordan.a.alkaff.android042019.services.action.PAUSE";

    // TODO: Rename parameters
    private static final String EXTRA_URL = "com.sdkjordan.a.alkaff.android042019.services.extra.URL";
    private static final String EXTRA_ID = "com.sdkjordan.a.alkaff.android042019.services.extra.ID";

    public MyIntentService3() {
        super("MyIntentService3");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionStart(Context context, String url) {
        Intent intent = new Intent(context, MyIntentService3.class);
        intent.setAction(ACTION_START);
        intent.putExtra(EXTRA_URL, url);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionStop(Context context, int id) {
        Intent intent = new Intent(context, MyIntentService3.class);
        intent.setAction(ACTION_STOP);
        intent.putExtra(EXTRA_ID, Integer.valueOf(id));
        context.startService(intent);
    }


    public static void startActionPause(Context context, int id) {
        Intent intent = new Intent(context, MyIntentService3.class);
        intent.setAction(ACTION_PAUSE);
        intent.putExtra(EXTRA_ID, Integer.valueOf(id));
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_START.equals(action)) {
                final String url = intent.getStringExtra(EXTRA_URL);
                handleActionStart(url);
            } else if (ACTION_STOP.equals(action)) {
                final String id = intent.getStringExtra(EXTRA_ID);
                handleActionStop(id);
            }else if (ACTION_PAUSE.equals(action)) {
                final String id = intent.getStringExtra(EXTRA_ID);
                handleActionPause(id);
            }

        }
    }



    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionStart(String url) {
        // TODO: Handle action Start
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionStop(String id) {
        // TODO: Handle action Stop
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void handleActionPause(String id) {
        // TODO: Handle action Pause
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
